module Pretty(pretty) where
-- | Import of other modules.
import Term

-- | Class for better viewing.
class Pretty a 
 where 
  pretty :: a -> String

-- | Instance for the view of terms.
instance Pretty Term 
 where
  pretty (Var  vName)    = vName  -- Print only the name of the variable
  pretty (Comb cName []) = cName  -- Print only the name of the empty
                                   -- combination
  -- Print the name of the combination and starts the better printing of the
  -- given other terms
  pretty (Comb cName cs) = cName ++ concatMap prettyTerm cs
   where
    -- | Helper function for the exactly representation with brackets and
    --   whitespaces.
    prettyTerm :: Term -> String
    prettyTerm (Var     v   ) = " " ++ v
    prettyTerm (Comb    c []) = " " ++ c
    prettyTerm (Comb    c ts) = " (" ++ c ++ (concatMap prettyTerm ts) ++ ")"