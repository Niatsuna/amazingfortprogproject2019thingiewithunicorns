module Matching where

-- | Import of other modules
import Term

import Substitution

-- | Matching

-- | Function which returns a substitution (wrapped in Maybe) between two terms.
--   If no substitution exists then the return value is Nothing.
--   If a substitution exist then the return value is (Just <substitution>).
--   A substitution can only exist if either 
--   1) both are identical variables,
--   2) the first term is only a variable, 
--   3) or both combinations show matching subterms.
match :: Term -> Term -> Maybe Subst
match (Var v1)         (Var v2) 
  | v1 == v2                    = Just identity
  | otherwise                   = Nothing
match (Var v1)         t        = (Just (single v1 t))
match (Comb _ _)       (Var _)  = Nothing
match (Comb c1 ls) (Comb c2 rs) 
  | c1 /= c2                    = Nothing
  | (length ls) == (length rs)  = foldr compM (Just identity) (combMatch ls rs)
  | otherwise                   = Nothing
 where
-- | Function which stores all possible matches between the subterms in a list.
  combMatch :: [Term] -> [Term] -> [Maybe Subst]
  combMatch []     _      = []
  combMatch _      []     = []
  combMatch (c:cs) (d:ds) = (match c d) : (combMatch cs ds)

-- | Function which compose two Maybe Subst to one.
  compM :: Maybe Subst -> Maybe Subst -> Maybe Subst
  compM Nothing  _         = Nothing
  compM _        Nothing         = Nothing
  compM (Just s) (Just t) = Just (compose s t)