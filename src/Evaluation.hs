module Evaluation 
    (Strategy ,loStrategy,liStrategy,
    roStrategy,riStrategy,
    poStrategy,piStrategy,
    reduceWith,evaluateWith) where
-- | Import of other modules.
import Data.List

import Term
import Prog

import Position
import Reductionssteps

-- | Evaluation

-- | Alias type for evaluation strategies.
type Strategy = Prog -> Term -> [Pos]

-- | Dataconstruct to identify the asked Strategy in 'applyStrat'
data StratLR =  LO 
              | LI
              | RI
              | RO

-- | Function which returns the leftmost outermost evaluation strategy.
loStrategy :: Strategy
loStrategy = (\p t -> case sortBy (applyStrat LO) (reduciblePos p t) of
                        [] -> []
                        xs -> [(head xs)])

-- | Function which returns the leftmost innermost evaluation strategy.
liStrategy :: Strategy
liStrategy = (\p t -> case sortBy (applyStrat LI) (reduciblePos p t) of
                       [] -> []
                       xs -> [(head xs)])

-- | Function which returns the rightmost outermost evaluation strategy.
roStrategy :: Strategy
roStrategy = (\p t -> case sortBy (applyStrat RO) (reduciblePos p t) of
                       [] -> []
                       xs -> [(head xs)])

-- | Function which returns the rightmost innermost evaluation strategy.
riStrategy :: Strategy
riStrategy = (\p t -> case sortBy (applyStrat RI) (reduciblePos p t) of
                       [] -> []
                       xs -> [(head xs)])

-- | Function which returns the parallel outermost evaluation strategy.
poStrategy :: Strategy
poStrategy 
  = (\p t -> filter (\x -> noAbove x (reduciblePos p t)) (reduciblePos p t))
 where
-- | Function which checks if the position has none reducible position above
--   itself.
  noAbove :: Pos -> [Pos] -> Bool
  noAbove _ []                            = True
  noAbove k (p:pos) | p /= k && above p k = False
                    | otherwise           = noAbove k pos

-- | Function which returns the parallel innermost evaluation strategy.
piStrategy :: Strategy
piStrategy 
  = (\p t -> filter (\x -> noBelow x (reduciblePos p t)) (reduciblePos p t))
 where
-- | Function which checks if the position has none reducible position below
--   itself.
  noBelow :: Pos -> [Pos] -> Bool
  noBelow _ []                            = True
  noBelow k (p:pos) | p /= k && below p k = False
                    | otherwise           = noBelow k pos

-- | Function which creates a ordering for 'sortBy' based on the asked 
--   strategy.
applyStrat :: StratLR -> Pos -> Pos -> Ordering
--  Leftmost
--  Outermost
applyStrat LO x y | (leftOf x y)  || (above x y) = LT --  leftmost or outermost
                  | (rightOf x y) || (below x y) = GT --  rightmost or innermost
                  | otherwise                    = EQ

--  Innermost
applyStrat LI x y | (leftOf x y)  || (below x y) = LT --  leftmost or innermost
                  | (rightOf x y) || (above x y) = GT --  rightmost or outermost
                  | otherwise                    = EQ
--  Rightmost
--  Outermost
applyStrat RO x y | (rightOf x y) || (above x y) = LT --  rightmost or outermost
                  | (leftOf x y)  || (below x y) = GT --  leftmost or innermost
                  | otherwise                    = EQ

--  Innermost
applyStrat RI x y | (rightOf x y) || (below x y) = LT --  rightmost or innermost
                  | (leftOf x y)  || (above x y) = GT --  leftmost or outermost
                  | otherwise                    = EQ


-- | Function which reduces with a given strategy and the given rules a term
--   on the first possible position.
reduceWith :: Strategy -> Prog -> Term -> Maybe Term
reduceWith _     (Prog []) _    = Nothing
reduceWith strat prog      term = case (strat prog term) of
                                    [] -> Nothing
                                    xs  -> (redAtPoses prog term xs)
 where
-- | Function which reduce with the given rules a term with the position sorted
--   by the given strategy.
  redAtPoses :: Prog -> Term -> [Pos] -> Maybe Term
  redAtPoses _  t []     = (Just t)
  redAtPoses pr t (p:ps) = case (reduceAt pr t p) of
                            Nothing -> Nothing
                            (Just rTerm) -> redAtPoses pr rTerm ps
  

-- | Function which evaluate a given term with the given strategy and programm.
evaluateWith :: Strategy -> Prog -> Term -> Term
evaluateWith _    (Prog []) t = t
evaluateWith strt  prg      t 
  | (isNormalForm  prg t)     = t
  | otherwise                 = evaluateWith' strt prg t (reduceWith strt prg t)
 where
-- | Function which manage the Maybe Term wraps to convert the result to a term.
 evaluateWith' :: Strategy -> Prog -> Term -> Maybe Term -> Term
 evaluateWith' strat prog t0 Nothing    = let (_:ps) = strat prog t0 in 
                                          (evaluateWith (\_ _ -> ps) prog t0)
 evaluateWith' strat prog _ (Just term) = evaluateWith strat prog term