module Examples where

-- | This is the standard Peano-structure
data Peano = Zero | Succ Peano

-- | This is a function, which just calls itself.
recursive :: Peano -> Peano
recursive x = recursive x

-- | This is a function, which multiplies a Peano number with zero (resulting 
--   in zero).
mul1 :: Peano -> Peano -> Peano
mul1 Zero m = Zero

-- | This is a function, which multiplies a Peano number with zero (resulting 
--   in zero).
mul2 :: Peano -> Peano -> Peano
mul2 m Zero = Zero

-- | This is a function, which returns always zero as a Peano number.
zeroing :: Peano -> Peano
zeroing x = Zero

iszero Zero = Zero

------Expressions--------------------------------------------------------------
-- LI, RI and PI do not terminate, but LO, RO and PO do:
-- mul1 Zero (recursive Zero)

-- LO does not terminate, but RO does::
-- mul2 (recursive x) (iszero (zeroing y))

-- RO does not terminate, but LO does:
-- mul1 (zeroing y) (recursive x)

-- RO and LO do not terminate, but PO does:
-- mul2 (mul2 (recursive x) (zeroing y)) (mul1 (zeroing x) (recursive y))

-- Nothing is terminating:
-- recursive x
