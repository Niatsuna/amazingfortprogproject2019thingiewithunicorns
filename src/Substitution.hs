{-# LANGUAGE TemplateHaskell #-}
module Substitution where

-- | Import of other modules
import Test.QuickCheck

import Term

-- | Substitution

-- | Dataconstructor for substitutions.
data Subst = Subst (VarName -> Term)

-- | Function which creates a Substitution which subsitute to itself.
identity :: Subst
identity =  Subst (\x -> (Var x) )

-- | Functon which creates a substitution from the given name to the given
--   term. If the substitution can't be applied to the given VarName the
--   term doesn't experience any changes.
single :: VarName -> Term -> Subst
single vName t = Subst (\v -> if v == vName  then t
                                             else (Var v) )

-- | Function which applies a given substitution on a specific term. If the
--   term has subterms then the substitution would be applied to each of these 
--   to.
apply :: Subst -> Term -> Term
apply (Subst f) (Var  vName)    = (f    vName)
apply sf        (Comb cName cs) = (Comb cName (map (apply sf) cs))

-- | Function which creates a substitution with a specific substituted term.
--   This specific substituted term was created through the use of two given
--   substitution after one another on the original term.
compose :: Subst -> Subst -> Subst
compose f (Subst g) = Subst (\x -> (apply f (g x) ) )

-- | QuickCheck

-- | Creates an instance which calculates random terms for quickCheck.
instance Arbitrary Term 
 where
  arbitrary = do  v  <- oneof[return "1",return "2",
                              return "3",return "4"]
                  c  <- oneof[return "add",return "sub",
                              return "mul",return "div"]
                  ts <- arbitrary :: Gen (Term)
                  oneof[return (Var v),return (Comb c [ts])]

-- | Creates an instance which calculates random substitutions for quickCheck.
instance Arbitrary Subst 
 where
  arbitrary = do  x  <- oneof[return "A",return "B",return "C",return "D"]
                  t  <- arbitrary :: Gen (Term)
                  return (single x t)

-- | Creates an instance to show substitution for failures at quickCheck.
instance Show Subst 
 where
  show _ = "<Substitution>"

-- | Function which let quickCheck test the 'apply identity' function
prop_ApplyIdentity :: Term -> Bool
prop_ApplyIdentity t = (apply identity t) == t

-- | Function which let quickCheck test the 'apply single' function with a
--   term with the same VarName as single.
prop_ApplySingle1 :: VarName -> Term -> Bool
prop_ApplySingle1 x t = apply (single x t) (Var x) == t
    
-- | Function which let quickCheck test the 'apply single' function with a
--   term with a different VarName as single.
prop_ApplySingle2 :: VarName -> VarName -> Term -> Property
prop_ApplySingle2 x y t = x /= y ==> apply (single x t) (Var y) == (Var y) 

-- | Function which let quickCheck test the 'apply s (Comb c ts)' function.
--   If the given term equals (Var v) then quickCheck test if the applied
--   substitution equals the function form the substitution given v.
prop_ApplySComb :: Subst -> Term -> Bool
prop_ApplySComb s         (Comb c ts) 
  = (apply s (Comb c ts))     == (Comb c (map (apply s) ts))
prop_ApplySComb (Subst f) (Var v)     
  = (apply (Subst f) (Var v)) == (f v)

-- | Function which let quickCheck test the commutative rule for compose with
--   the given example of a given substitution and identity.
prop_ComposeIdentityS :: Subst -> Term -> Bool
prop_ComposeIdentityS s t 
  = (apply (compose identity s) t) == (apply (compose s identity) t)

-- | Funtion which let quickCheck test the 'apply compose' function with two
--   given substitution.
prop_ComposeS1S2 :: Subst -> Subst -> Term -> Bool
prop_ComposeS1S2  s1 s2 t 
  = (apply (compose s2 s1) t) == (apply s2 (apply s1 t))

-- | Let the user run all test with one command.
return []
runTests :: IO Bool
runTests = $quickCheckAll