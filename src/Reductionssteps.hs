module Reductionssteps where
-- | Import of other modules
import Term
import Prog

import Matching
import Position
import Substitution

-- | Reductionssteps

-- | Function which search in the given programm a rule which can match with the
--   given term. If there is such a rule the return value equals a tuple with 
--   the righ-hand-side part of the rule and the specific substitution.
--   If there is no such rule the return value is Nothing.
findRule :: Prog -> Term -> Maybe (Rhs,Subst)
findRule (Prog []) _               = Nothing
findRule (Prog ((Rule l r):rs)) t0 = let subst = (match l t0) in
                                     case subst of
                                      Nothing -> findRule (Prog rs) t0
                                      Just s  -> Just (r,s)

-- | Function which reduce a part of the term at a given position with the given
--   rules within the programm.
reduceAt :: Prog -> Term -> Pos -> Maybe Term
reduceAt pr t ps = reduceAt' t ps (maybeRhsToTerm (findRule pr (selectAt t ps)))
 where
-- | Function which extract the righ-hand-side rule out of the tuple for the
--   substitution and if it's not existing return Nothing
  maybeRhsToTerm :: Maybe(Rhs, Subst) -> Maybe Term
  maybeRhsToTerm (Just (rhs, subst)) = Just (apply subst rhs)
  maybeRhsToTerm Nothing             = Nothing

-- | Function which replace the substituted term in the original term.
  reduceAt' :: Term -> Pos -> Maybe Term -> Maybe Term
  reduceAt' _    _ Nothing     = Nothing
  reduceAt' term p (Just mt)   = (Just (replaceAt term p mt))

-- | Function which returns all positions which are reducible.
reduciblePos :: Prog -> Term -> [Pos]
reduciblePos (Prog []) _    = []
reduciblePos pr        term = filterPos pr term (allPos term)
 where
-- | Function which filter all non-reducible position out of the result.
  filterPos :: Prog -> Term -> [Pos] -> [Pos]
  filterPos prog t pos = filter (\p -> maybeToBool (reduceAt prog t p)) pos

 -- | Function which converts the Maybe Term into a bool for filterPos.
  maybeToBool :: Maybe Term -> Bool
  maybeToBool Nothing   = False
  maybeToBool (Just _)  = True

-- | Functions which checks if a term isn't reducible with the given rules.
isNormalForm :: Prog -> Term -> Bool
isNormalForm prog t = (reduciblePos prog t) == []