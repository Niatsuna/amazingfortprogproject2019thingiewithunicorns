module Position where

-- | Import of other modules
import Term

-- | Position
-- | Type for the representation of position from subterms within a term.
type Pos = [Int]

-- | Function which checks if a given position is above another given position
--   which means that the term with the second position is a subterm 
--   (or a subterm of one or multiple subterms) of the term 
--   with the first position
above :: Pos -> Pos -> Bool
above  []       _                   = True --  The 'root' is above all.
above  _       []                   = False --  The 'root' can't be below 
                                            --  anything.
above (x:pos1) (y:pos2) | x == y    = above pos1 pos2
                        | otherwise = False

-- | Functon which checks if the given position is below another given position
--   which means the the term with the second position is a supterm (or a 
--   supterm of the direct supterm) of the term with the first position.
below :: Pos -> Pos -> Bool
below  []        _                  = False --  The 'root' can't be below 
                                            --  anything.
below  _        []                  = True  --  Everything is below the 'root'
below (x:pos1) (y:pos2) | x == y    = below pos1 pos2
                        | otherwise = False

-- | Function which checks if the given position is left of another given
--   position which means both position are on the same level 
--   (same supterm / same length) but the first position has a lower index.
leftOf :: Pos -> Pos -> Bool
leftOf  []       _                   = False --  2nd position is below / equal
leftOf  _       []                   = False --  1st position is below / equal
leftOf (x:pos1) (y:pos2) | x == y    = leftOf pos1 pos2
                         | x <  y    = True
                         | otherwise = False

-- | Function which checks if the given position is right of another given
--   position which means both position are on the same level
--   (same supterm / same length) but the second position has a lower index.
rightOf :: Pos -> Pos -> Bool
rightOf  []       _                   = False --  2nd position is below / equal
rightOf  _       []                   = False --  1st position is below / equal
rightOf (x:pos1) (y:pos2) | x == y    = rightOf pos1 pos2
                          | x >  y    = True
                          | otherwise = False

-- | Function which returns the (sub-)term at the given position.
selectAt :: Term -> Pos -> Term
selectAt t                   [] = t --  No (remaining) position
selectAt (Var  _)             _ = error ("Illegal Term")
selectAt (Comb _     [])      _ = error ("Illegal Term")
selectAt (Comb cName (c:cs)) (p:pos) 
  | p == 1                      = selectAt c pos
  | p >  1                      = selectAt (Comb cName cs) ((p-1):pos)
  | otherwise                   = error ("Illegal Position")

-- | Function which replaces a part of a term at a given position with another
--   given term. If there is no given position the whole term should be
--   replaced. After the replacing the function returns the new whole term
--   with the replaced part.
replaceAt :: Term -> Pos -> Term -> Term
replaceAt _               [] tp = tp
replaceAt (Var _)         _  _  = error ("Illegal starting Term")
replaceAt (Comb _     []) _  _  = error ("Illegal starting Term")
replaceAt (Comb cName ts) ps tp = (Comb cName (repSubT ts ps tp))
 where
-- | Function which helps replaceAt to check each existing subterm for the given
--   position and reconstruct the remaining non-replaced term around the 
--   replaced term.
 repSubT :: [Term] -> Pos -> Term -> [Term]
 repSubT []     _       _              = [] --  No terms
 repSubT (_:cs) []      t              = (t:cs) --  No (remaining) position
 repSubT (c:cs) (p:pos) t  | p == 1    = (replaceAt c pos t):cs
                           | p >  1    = c:(repSubT cs ((p-1):pos) t)
                           | otherwise = error ("Illegal Position")

-- | Function which returns all position in a term. 
--   This counts position of subterms and all position of subterms 
--   of said subterms.
allPos :: Term -> [Pos]
allPos (Var _)     = [[]] --  No existing subterms equals the only position is 
                          --  the position of the original term itself (root)
allPos (Comb _ ts) = [] : (allSide ts 1)
 where
-- | Function which returns all position of the terms that are on the 
--   same level and calls allDown to get the subterms of these terms.
  allSide :: [Term] -> Int -> [Pos] 
  allSide []               _  = []
  allSide ((Var _):cs)     n  = [n] : (allSide cs (n+1))
  allSide ((Comb _ us):cs) n  = ([n] : (allDown n 1 us)) ++ (allSide cs (n+1))

-- | Function which gets all position of the subterms from the 
--   position allSide is at the moment. Calls allSide on the level
--   of the current subterms because all subterms are on the same level.
  allDown :: Int -> Int -> [Term] ->  [Pos]
  allDown _  _  [] = []
  allDown m  x  xs = map (m:) (allSide xs x)