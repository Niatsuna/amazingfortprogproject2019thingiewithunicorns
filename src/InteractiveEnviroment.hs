module InteractiveEnviroment where

-- | Import of other modules
import Parser
import Prog
import Term

import Evaluation
import Pretty

-- | Interactive Enviroment

-- | Function which starts the interactive Enviroment.
main :: IO ()
main = greeting >> (main' loStrategy ("",(Prog [])))
 where
  -- | Function which get the user input and calls reactToCmd.
  main' :: Strategy -> (FilePath,Prog) -> IO ()
  main' strt (pp,pg) | pp == "" = putStr("[SSHI]» ") 
                                  >> getLine 
                                  >>= (\input -> reactToCmd input strt (pp,pg))
                     | otherwise = putStr("[" ++ pp ++ "]» ")
                                  >> getLine 
                                  >>= (\input -> reactToCmd input strt (pp,pg))

  -- | Function which reacts to the specific input from the user.
  reactToCmd :: String -> Strategy -> (FilePath,Prog) -> IO ()
  reactToCmd []    strt fp = main' strt fp
  reactToCmd input strt (f,p)
   | (head cmd) == ':' 
   = case cmd of
  --  Print help message
      ":h"      -> help >> main' strt (f,p)
      ":help"   -> help >> main' strt (f,p)

  --  Quit interpreter
      ":q"      -> prLn("Ok. Leaving. Bye ~")
      ":quit"   -> prLn("Ok. Leaving. Bye ~")

  --  (Un-)loades program
      ":l"      -> load args strt
      ":load"   -> load args strt

  --  Reloades the current program
      ":r"      -> load [f] strt
      ":reload" -> load [f] strt

  --  Set the strategy 
      ":s"      -> let (msg,st) = (set args strt) in 
                   prLn(msg) >> main' st (f,p)
      ":set"    -> let (msg,st) = (set args strt) in 
                   prLn(msg) >> main' st (f,p)

  --  React to unknown or not defined commands
      _         -> prLn("Unknown command! Type <:h[elp]> for help!") 
                   >> main' strt (f,p)
    | otherwise
    = (expression (parse (concatMap (++" ") (cmd:args))) strt p) 
       >> main' strt (f,p)
    where
      (cmd:args) = (words input)

-- | Function which prints a welcome message when the interactive 
--   enviroment was started.
  greeting :: IO ()
  greeting = putStrLn( concatMap (++['\n']) welcome )

-- | Function which reacts to the given arguments and returns a tuple with 
--   a corresponding message and the specific evaluation strategy. If there is
--   no argument or the argument is invalid the strategy would not change.
  set :: [String] -> Strategy -> (String,Strategy)
  set s st | (s == []) = ("Too few arguments! Type <:h[elp]> for help",st)
           | otherwise = let (x:_) = s in 
              case x of
                  "li"      -> ("Strategy is now: LI",liStrategy)
                  "lo"      -> ("Strategy is now: LO",loStrategy)
                  "ri"      -> ("Strategy is now: RI",riStrategy)
                  "ro"      -> ("Strategy is now: RO",roStrategy)
                  "pi"      -> ("Strategy is now: PI",piStrategy)
                  "po"      -> ("Strategy is now: PO",poStrategy)
                  _         -> ("Invalid strategy shortcut",st)

-- | Function which unloads the current programm or loads a new programm.
  load :: [String] -> Strategy -> IO ()
  load []    strt = prLn("Unloaded!") >> main' strt ("",(Prog [])) --  Unload
  load (a:_) strt = do  prLn("Loading...")
                        file <- parseFile a
                        case file of
                          (Left lError) -> prLn(lError) 
                                            >> main' strt ("",(Prog []))
                          (Right prog)  -> prLn("Loading complete!") 
                                            >> main' strt (a,prog)

-- | Function which evaluates given expressions with the given strategy and
--   rules and prints out the result.
  expression :: Either String Term -> Strategy -> Prog -> IO ()
  expression (Left  eFail) _  _    = prLn eFail
  expression (Right term)  st pr   = putStrLn (pretty( evaluateWith st pr term ))

-- | Function which prints the help message after called.
  help :: IO ()
  help = prLn ( 
    "Commands available from the prompt:\n" ++
    "<expression>       Evaluates the specified expression.\n" ++
    ":h[elp]            Shows this help message.\n" ++ 
    ":l[oad] <file>     Loads the specified file.\n" ++
    ":l[oad]            Unloads the currently loaded file.\n" ++
    ":r[eload]          Reloads the lastly loaded file.\n" ++
    ":s[et] <strategy>  Sets the specified evaluation strategy\n" ++
    "                   where <strategy> is one of 'lo', 'li',\n" ++
    "                   'ro', 'ri', 'po' or 'pi'.\n" ++
    ":q[uit]            Exits the interactive environment.")

-- | Function which stores the welcome message / header.
  welcome :: [String]
  welcome = ["   _________     _________     __      __      __",
             "  |   _____|    |   _____|    |  |    |  |    |  |",
             " |   |____     |   |____      |  |____|  |    |  |",
             "  |_____   |    |_____   |    |   ____   |    |  |",
             "  ______|   |   ______|   |   |  |    |  |    |  |",
             "  |________|    |________|    |__|    |__|    |__|",
            "    SUPER         SPECIAL       HASKELL   INTERPRETER",
            "------------------------------------------------------",
            "     Welcome to SSHI! Type <:h[elp]> for help!       "]

 -- | Function which print out the given String with a given prefix.
  prLn :: String -> IO()
  prLn str = putStrLn("[SSHI]» " ++ str)